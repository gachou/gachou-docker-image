FROM node:10

ENV APP_HOME /gachou/app

RUN apt-get update && apt-get install unzip
ADD https://github.com/tianon/gosu/releases/download/1.10/gosu-amd64 /usr/local/bin/gosu
RUN chmod a+x /usr/local/bin/gosu
# From: https://gist.github.com/alkrauss48/2dd9f9d84ed6ebff9240ccfa49a80662
# Set the home directory to our app user's home.
RUN mkdir -p $APP_HOME

WORKDIR $APP_HOME

RUN npm install @gachou/gachou-backend
ADD tmp/gachou-frontend-download/gachou-frontend.zip /gachou-frontend.zip
RUN unzip /gachou-frontend.zip && mv dist static
ADD ../gachou-backend/docker $APP_HOME/docker

VOLUME /gachou/media
VOLUME /gachou/cache
EXPOSE 8080
ENTRYPOINT ["/gachou/app/docker/entrypoint.sh"]
CMD ["/usr/local/bin/node", "node_modules/.bin/gachou-backend", "--config", "docker/config.js"]
