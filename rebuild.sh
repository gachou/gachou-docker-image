#!/bin/bash

cd "$(dirname "$(readlink -f "$0")")" || exit 1

FRONTEND_DIR=tmp/gachou-frontend-download

rm -rf "${FRONTEND_DIR}"
mkdir -p "${FRONTEND_DIR}"
wget -O "${FRONTEND_DIR}/gachou-frontend.zip" "https://gitlab.com/api/v4/projects/gachou%2Fgachou-frontend/jobs/artifacts/master/download?job=build"

docker-compose build
